export type AuthRequestBody = {
  username: string,
  password: string
}