export type AuthResponseBody = {
  token: string
}
